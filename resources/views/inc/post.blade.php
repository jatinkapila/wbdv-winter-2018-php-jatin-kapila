@if (count($errors)> 0)
  @foreach ($errors->all() as $error)
    <div class="alert alert-danger">
      @php
      echo $error;
      @endphp

    </div>

  @endforeach

@endif

@if (session('success'))
  <div class="alert alert-success">
    @php
      echo session('success')."<br/>";
      echo "<br/>";
      echo "'Like' or 'Unlike' the posts"."<br />";
    @endphp

    <a href="/about"> like </a>&nbsp&nbsp&nbsp
    <a href="/"> unlike </a>

  </div>
@endif
