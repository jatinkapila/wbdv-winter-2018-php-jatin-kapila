@extends('layouts.app1')

@section('content')
  <br>
  <br>
  <br>
  <br>
  <br>

  <h1>Contact</h1>
  {!! Form::open(['url' => 'contact/submit']) !!}
    {{-- //using bootstrap --}}
    <div class="form-group">
    @php
      echo Form::label('name', 'Name');
      echo Form::text('name', '',['class'=>'form-control','placeholder'=>'enter name']);

    @endphp
    </div>
    <div class="form-group">
    @php
      echo Form::label('email', 'E-Mail Address');
      echo Form::text('email', '',['class'=>'form-control','placeholder'=>'enter email']);
    @endphp
    </div>
    <div class="form-group">
    @php
      echo Form::label('message', 'Post');
      echo Form::textarea('message', '',['class'=>'form-control','placeholder'=>'enter post']);
    @endphp
    </div>
    <div>
      @php
        echo Form::submit('Submit',['class'=>'btn btn-primary']);
      @endphp


    </div>

    {!! Form::close() !!}

@endsection
