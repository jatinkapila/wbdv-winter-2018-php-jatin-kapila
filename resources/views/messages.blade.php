@extends('layouts.app1')

@section('content')
  <h1>Posts</h1>
  @if(count($messages) > 0)
    @foreach($messages as $message)
      <ul class="list-group">
        <li class="list-group-item">Name: {{$message->name}}</li>
        <li class="list-group-item">Email: {{$message->email}}</li>
        <li class="list-group-item">Message: {{$message->message}}</li>
      </ul>
      @include('inc.post')
    @endforeach
  @endif
@endsection

@section('sidebar')
  @parent
  <p>This is appended to the sidebar</p>
@endsection
