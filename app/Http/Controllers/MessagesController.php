<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class MessagesController extends Controller
{
    public function submit(Request $request){
      # code...
      $this->validate($request,[
        'name' => 'required',
        'email' => 'required',
        'message'=>'required'

      ]);
      //create a message post
      $message = new Post;
      $message->name = $request->input('name');
      $message->email = $request->input('email');
      $message->message = $request->input('message');
      $message->save();

      return redirect('/messages')->with('success','Post Sent');

    }

      # code...
      public function getMessages(){
        $messages = Post::all();

        return view('/messages')->with('messages', $messages);
      }
}
